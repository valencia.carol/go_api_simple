# Rest api in Golang

## Setting in Docker

```
make build
make run
```

## Testing

```
curl http://localhost:8080/
curl http://localhost:8080/events
curl http://localhost:8080/events/1
```

## Resources

https://medium.com/the-andela-way/build-a-restful-json-api-with-golang-85a83420c9da
